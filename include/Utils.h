//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : Utils.h                                                       //
//  Project   : glypher                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
// mcow
#include "mcow/StringUtils/StringUtils.hpp"

//----------------------------------------------------------------------------//
// Macros                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
#define VERIFY(_exp_, _fmt_, ...)       \
    do {                                \
        if(!(_exp_)) {                  \
            exit(1);                    \
        }                               \
    } while(0)


//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//


//------------------------------------------------------------------------------
inline std::string
BuildStringFromCharRange(char min, char max)
{
    std::string str;
    for(char c = min; c <= max; ++c) {
        str += mcow::StringUtils::ToString(c);
    }
    return str;
}

//------------------------------------------------------------------------------
inline std::string
BuildAsciiString()
{
    return BuildStringFromCharRange(32, 126);
}

//------------------------------------------------------------------------------
inline std::string
BuildLowerAlphaString()
{
    return BuildStringFromCharRange('a', 'z');
}

//------------------------------------------------------------------------------
inline std::string
BuildUpperAlphaString()
{
    return BuildStringFromCharRange('A', 'Z');
}

//------------------------------------------------------------------------------
inline std::string
BuildAlphaString()
{
    mcow::StringUtils::Concat(
        BuildUpperAlphaString(),
        BuildLowerAlphaString()
    );
}
//------------------------------------------------------------------------------
inline std::string
BuildDigitsString()
{
    return BuildStringFromCharRange('0', '9');
}

//------------------------------------------------------------------------------
inline std::string
BuildSpecialString()
{
    return mcow::StringUtils::Concat(
        BuildStringFromCharRange( 32,  47),
        BuildStringFromCharRange( 58,  64),
        BuildStringFromCharRange( 91,  96),
        BuildStringFromCharRange(123, 126)
    );
}





//------------------------------------------------------------------------------
struct Rect_t {
    uint32_t x, y, w, h;
}; // struct Rect_t
