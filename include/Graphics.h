#pragma once
// std
#include <vector>
#include <string>
// glypher
#include "../include/Utils.h"

//----------------------------------------------------------------------------//
// Forward declaration.                                                       //
//----------------------------------------------------------------------------//
class FontMapInfo;


namespace Graphics {
//----------------------------------------------------------------------------//
// Opaque Types                                                               //
//----------------------------------------------------------------------------//
struct Font_t;    // Opaque type.
struct Texture_t; // Opaque type.

//----------------------------------------------------------------------------//
// Lifecycle                                                                  //
//----------------------------------------------------------------------------//
void Initialize();
void Shutdown  ();

//----------------------------------------------------------------------------//
// Window                                                                     //
//----------------------------------------------------------------------------//
bool IsWindowOpen();

void WindowRunEvents();
void WindowClear();
void WindowPresent(Texture_t *pTexture);

//----------------------------------------------------------------------------//
// Font                                                                       //
//----------------------------------------------------------------------------//
Font_t* LoadFont(std::string const &filename, uint8_t fontSize);
void    DestroyFont(Font_t *pFont);

//----------------------------------------------------------------------------//
// Texture                                                                    //
//----------------------------------------------------------------------------//
void GetTextureSize(
    Texture_t *pTexture,
    uint32_t *pWidthOut,
    uint32_t *pHeightOut);

Texture_t* CreateTexture(uint32_t width, uint32_t height);
void DestroyTexture(Texture_t *pTexture);

void RenderTextureTo(
    Texture_t *pSrcTexture,
    uint32_t x, uint32_t y,
    Texture_t *pDstTexture);

void SaveTextureToFile(Texture_t *pTexture, std::string const &filename);


//----------------------------------------------------------------------------//
// Glyphs                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
struct GlyphInfo_t {
    // Char.
    char c;
    // Metrics.
    int min_x;
    int max_x;
    int min_y;
    int max_y;
    int advance;
    // Font.
    int     font_accent;
    Font_t *p_font = nullptr;
}; // struct GlyphInfo_t

GlyphInfo_t* CreateGlyphInfo(char c, Font_t *pFont);
void DestroyGlyphInfo(GlyphInfo_t *pGlyphInfo);

//------------------------------------------------------------------------------
struct Glyph_t {
    GlyphInfo_t *p_info    = nullptr;
    Texture_t   *p_texture = nullptr;

    Rect_t rect;
}; // struct Glyph_t

Texture_t* CreateGlyphTexture(GlyphInfo_t *pGlyphInfo);

} // namespace Graphics
