//----------------------------------------------------------------------------//
//                       __      __                  __   __                  //
//               .-----.|  |_.--|  |.--------.---.-.|  |_|  |_                //
//               |__ --||   _|  _  ||        |  _  ||   _|   _|               //
//               |_____||____|_____||__|__|__|___._||____|____|               //
//                                                                            //
//  File      : CmdLineParser.h                                               //
//  Project   : glypher                                                           //
//  Date      : Sep 25, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//----------------------------------------------------------------------------//

#pragma once
// std
#include <string>
#include <vector>

//----------------------------------------------------------------------------//
// CmdLineParser                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
class CmdLineParser
{
    //------------------------------------------------------------------------//
    // Enums / Constants Typedefs                                             //
    //------------------------------------------------------------------------//
public:
    //--------------------------------------------------------------------------
    enum class ValueType {
        No, Required, Optional
    };

    //--------------------------------------------------------------------------
    enum class ArgType {
        Long, Short, NonFlag
    };

    //------------------------------------------------------------------------//
    // Inner Types                                                            //
    //------------------------------------------------------------------------//
public:
    //--------------------------------------------------------------------------
    struct Flag
    {
        //--------------------------------------------------------------------//
        // CTOR / DTOR                                                        //
        //--------------------------------------------------------------------//
        Flag(
            std::string const  &longName,
            std::string const  &shortName,
            ValueType           valueType,
            std::string const  &helpString)
            : longName  (longName)
            , shortName (shortName)
            , valueType (valueType)
            , helpString(helpString)
            , found     (false)
        {
            // Empty...
        }

        //--------------------------------------------------------------------//
        // iVars                                                              //
        //--------------------------------------------------------------------//
        std::string longName;
        std::string shortName;
        ValueType   valueType;
        std::string helpString;
        std::string value;
        bool        found;
    }; // struct Flag


    //------------------------------------------------------------------------//
    // CTOR / DTOR                                                            //
    //------------------------------------------------------------------------//
public:
    //--------------------------------------------------------------------------
    CmdLineParser(std::vector<Flag> const &flags)
        : m_flags(flags)
    {
        // Sanitize...
    }

    //------------------------------------------------------------------------//
    // Public Methods                                                         //
    //------------------------------------------------------------------------//
public:
    //--------------------------------------------------------------------------
    void Parse(int argc, char const * const argv[])
    {
        auto args = std::vector<std::string>(argv + 1, argv + argc);
        Parse(args);
    }

    void Parse(std::vector<std::string> const &args);


    //--------------------------------------------------------------------------
    Flag& GetFlag(std::string const &name);

    const Flag& GetFlag(std::string const &name) const
    {
        return const_cast<CmdLineParser*>(this)->GetFlag(name);
    }


    //--------------------------------------------------------------------------
    std::vector<std::string> const& GetNonFlagArgs() const
    {
        return m_nonFlags;
    }

    //--------------------------------------------------------------------------
    bool Found(std::string const &name) const
    {
        return GetFlag(name).found;
    }

    //--------------------------------------------------------------------------
    std::string GenerateHelpString(std::string const &programName) const;




    //------------------------------------------------------------------------//
    // Private Methods                                                        //
    //------------------------------------------------------------------------//
private:
    //--------------------------------------------------------------------------
    std::string _CleanName(std::string const &name) const;

    //--------------------------------------------------------------------------
    ArgType _GetArgType(std::string const &arg) const;

    //--------------------------------------------------------------------------
    void _FatalError_ArgumentRequired(Flag const &flag) const;


    //------------------------------------------------------------------------//
    // iVars                                                                  //
    //------------------------------------------------------------------------//
private:
    std::vector<Flag>        m_flags;
    std::vector<std::string> m_nonFlags;
}; // CmdLineParser
