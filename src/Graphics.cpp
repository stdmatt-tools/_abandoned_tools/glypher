// Header
#include "../include/Graphics.h"
// SDL
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
// mcow
#include "mcow/PointerUtils/Assign.hpp"
#include "mcow/StringUtils/StringUtils.hpp"
#include "mcow/CodeUtils/FakeKeywords.hpp"
// glypher
#include "../include/Utils.h"

//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
constexpr auto PIXEL_FORMAT = SDL_PIXELFORMAT_RGBA8888;

//----------------------------------------------------------------------------//
// Private Vars                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
SDL_Window   *g_pWindow    = nullptr;
SDL_Renderer *g_pRenderer  = nullptr;
bool          g_WindowOpen = false;

//----------------------------------------------------------------------------//
// MACROS                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------


//template <typename Src, typename Dst>
//Dst* CastSDL(Src *pSrc) {
//    return (Dst*)pSrc;
//}

#define _CAST_SDL(_sdl_type_, _glypher_type_)    \
\
    _sdl_type_ CastSDL(_glypher_type_ pSrc) {  \
        return (_sdl_type_)pSrc;                       \
    }                                            \
  \
    _glypher_type_ CastSDL(_sdl_type_ pSrc) {  \
        return (_glypher_type_)pSrc;                       \
    }

//------------------------------------------------------------------------------
_CAST_SDL(SDL_Texture*, Graphics::Texture_t*);
_CAST_SDL(TTF_Font*,    Graphics::Font_t*);


//----------------------------------------------------------------------------//
// Private Functions                                                          //
//----------------------------------------------------------------------------//
static SDL_Surface*
_CreateSurface(uint32_t width, uint32_t height)
{
    int bpp;
    uint32_t red_mask   = 0;
    uint32_t green_mask = 0;
    uint32_t blue_mask  = 0;
    uint32_t alpha_mask = 0;

    SDL_PixelFormatEnumToMasks(
        SDL_PIXELFORMAT_ARGB8888,
        &bpp,
        &red_mask,
        &green_mask,
        &blue_mask,
        &alpha_mask
    );

    auto p_surface = SDL_CreateRGBSurface(
        0,
        width,
        height,
        bpp,
        red_mask,
        green_mask,
        blue_mask,
        alpha_mask
    );

    VERIFY(
        p_surface,
        "Failed to create surface - width: (%d) - height: (%d) - SDL_Error(%s)",
        width,
        height,
        SDL_GetError()
    );

    return p_surface;
}

//------------------------------------------------------------------------------
static void
_SafeDestroySurface(SDL_Surface *&pSurface)
{
    if(pSurface) {
        SDL_FreeSurface(pSurface);
        pSurface = nullptr;
    }
}


//----------------------------------------------------------------------------//
// Lifecycle                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Graphics::Initialize()
{
    //
    // SDL
    VERIFY(
        SDL_Init(SDL_INIT_EVERYTHING) == 0,
        "Error initializing SDL - (%s)",
        SDL_GetError()
    );

    //
    // SDL_Image
    const auto flags = (IMG_INIT_JPG | IMG_INIT_PNG);
    VERIFY(
        IMG_Init(flags) == flags,
        "Error initializing SDL_image - (%s)",
        IMG_GetError()
    );

    //
    // SDL_TTF
    VERIFY(
        TTF_Init() != -1,
        "Error initializing SDL_ttf - (%s)",
        TTF_GetError()
    );

    //
    // Window and Renderer
    VERIFY(
        SDL_CreateWindowAndRenderer(
            800,
            800,
            SDL_WINDOW_SHOWN,
            &g_pWindow,
            &g_pRenderer
        ) == 0,
        "Failed to create SDL window and renderer - (%s)",
        SDL_GetError()
    );
    g_WindowOpen = true;
}

//------------------------------------------------------------------------------
void
Graphics::Shutdown()
{

}

//----------------------------------------------------------------------------//
// Window                                                                     //
//----------------------------------------------------------------------------//
bool
Graphics::IsWindowOpen()
{
    return g_WindowOpen;
}

//------------------------------------------------------------------------------
void
Graphics::WindowRunEvents()
{
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        if(event.type == SDL_QUIT) {
            g_WindowOpen = false;
        }
    }
}

//------------------------------------------------------------------------------
void
Graphics::WindowClear()
{
    SDL_SetRenderDrawColor(g_pRenderer, 0, 0, 0, 0);
    SDL_RenderClear(g_pRenderer);
}

//------------------------------------------------------------------------------
void
Graphics::WindowPresent(Texture_t *pTexture)
{
    auto p_sdl_texture = (SDL_Texture*)pTexture;

    int width, height;
    SDL_QueryTexture(p_sdl_texture, nullptr, nullptr, &width, &height);

    SDL_Rect dst_rect;
    dst_rect.x = 0;
    dst_rect.y = 0;
    dst_rect.w = width;
    dst_rect.h = height;

    SDL_RenderCopy(g_pRenderer, p_sdl_texture, nullptr, &dst_rect);
    SDL_RenderPresent(g_pRenderer);
}


//----------------------------------------------------------------------------//
// Font                                                                       //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
Graphics::Font_t*
Graphics::LoadFont(std::string const &filename, uint8_t fontSize)
{
    auto *p_font = TTF_OpenFont(filename.c_str(), fontSize);
    VERIFY(p_font, "Failed to load font - filename: (%s) - size: (%d)", filename, size);
    return CastSDL(p_font);
}

//------------------------------------------------------------------------------
void
Graphics::DestroyFont(Font_t *pFont)
{
    TTF_CloseFont((TTF_Font *)pFont);
}


//----------------------------------------------------------------------------//
// Texture                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Graphics::GetTextureSize(
    Texture_t *pTexture,
    uint32_t *pWidthOut,
    uint32_t *pHeightOut)
{
    int w, h;
    SDL_QueryTexture(CastSDL(pTexture), nullptr, nullptr, &w, &h);
    MCOW_SAFE_ASSIGN(pWidthOut,  w);
    MCOW_SAFE_ASSIGN(pHeightOut, h);
}

//------------------------------------------------------------------------------
Graphics::Texture_t*
Graphics::CreateTexture(uint32_t width, uint32_t height)
{
    auto p_sdl_texture = SDL_CreateTexture(
        g_pRenderer,
        PIXEL_FORMAT,
        SDL_TEXTUREACCESS_TARGET,
        width,
        height
    );

    return CastSDL(p_sdl_texture);
}

//------------------------------------------------------------------------------
void
Graphics::DestroyTexture(Texture_t *pTexture)
{
    auto *p_sdl_texture = CastSDL(pTexture);
    if(p_sdl_texture) {
        SDL_DestroyTexture(p_sdl_texture);
    }
}

void
Graphics::RenderTextureTo(
    Texture_t *pSrcTexture,
    uint32_t x, uint32_t y,
    Texture_t *pDstTexture)
{
    //         SDL_RenderCopy(g_pRenderer, p_img_texture, &src_rect, &dst_rect);
    SDL_Rect src_rect;
    SDL_Rect dst_rect;

    uint32_t w, h;
    Graphics::GetTextureSize(pSrcTexture, &w, &h);
    src_rect.x = 0;
    src_rect.y = 0;
    src_rect.w = w;
    src_rect.h = h;

    dst_rect.x = x;
    dst_rect.y = y;
    dst_rect.w = src_rect.w;
    dst_rect.h = src_rect.h;

    SDL_SetRenderTarget(g_pRenderer, CastSDL(pDstTexture));
    SDL_RenderCopy(g_pRenderer, CastSDL(pSrcTexture), &src_rect, &dst_rect);
    SDL_SetRenderTarget(g_pRenderer, nullptr);
}

// //------------------------------------------------------------------------------
// SDL_Texture*
// Graphics::GenerateOutputTexture(
//     std::vector<Image> const &images,
//     uint32_t width,
//     uint32_t height)
// {
//     // Create the texture.
//     auto p_output_texture = SDL_CreateTexture(
//         g_pRenderer,
//         SDL_PIXELFORMAT_RGBA8888,
//         SDL_TEXTUREACCESS_TARGET,
//         width,
//         height
//     );
//     VERIFY(
//         p_output_texture,
//         "Failed to create the screen texture - (%s)",
//         SDL_GetError()
//     );

//     // Clear the texture.
//     SDL_SetRenderTarget(g_pRenderer, p_output_texture);
// //    SDL_SetRenderDrawBlendMode(g_pRenderer, SDL_BLENDMODE_BLEND);
// //    SDL_SetRenderDrawColor(g_pRenderer, 0, 0, 255, 0);
// //    SDL_RenderClear(g_pRenderer);

//     // Render the images.
//     for(auto const &image : images) {
//         SDL_Rect src_rect;
//         src_rect.x = 0;
//         src_rect.y = 0;
//         src_rect.w = image.rect.w;
//         src_rect.h = image.rect.h;

//         SDL_Rect dst_rect;
//         dst_rect.x = image.rect.x;
//         dst_rect.y = image.rect.y;
//         dst_rect.w = image.rect.w;
//         dst_rect.h = image.rect.h;

//         SDL_Texture *p_img_texture = g_TexturesMap[image.textureId];
// //        SDL_SetTextureColorMod(p_output_texture, 0, 0, 255);
//         SDL_SetTextureBlendMode(p_img_texture, SDL_BLENDMODE_BLEND);
//         SDL_RenderCopy(g_pRenderer, p_img_texture, &src_rect, &dst_rect);

//         // SDL_SetRenderDrawColor(g_pRenderer, 255, 0, 255, 0xFF);
//         // SDL_RenderDrawRect(g_pRenderer, &dst_rect);
//     }

//     SDL_SetRenderTarget(g_pRenderer, nullptr);
//     return p_output_texture;
// }


//------------------------------------------------------------------------------
void
Graphics::SaveTextureToFile(Texture_t *pTexture, std::string const &filename)
{
    auto *p_sdl_texture = CastSDL(pTexture);
    SDL_SetRenderTarget(g_pRenderer, p_sdl_texture);


    uint32_t width, height;
    Graphics::GetTextureSize(pTexture, &width, &height);

    auto p_surface = _CreateSurface(width, height);
    SDL_RenderReadPixels(
        g_pRenderer,
        nullptr,
        PIXEL_FORMAT,
        p_surface->pixels,
        p_surface->pitch
    );

    IMG_SavePNG(p_surface, filename.c_str());
    _SafeDestroySurface(p_surface);

    SDL_SetRenderTarget(g_pRenderer, nullptr);
}



//----------------------------------------------------------------------------//
// Glyphs                                                                     //
//----------------------------------------------------------------------------//
// //------------------------------------------------------------------------------
// Graphics::Texture_t*
// Graphics::RenderGlyphs(FontMapInfo const *pFontMapInfo)
// {
//     auto p_ttf_font = CastSDL(pFontMapInfo->GetFont());
//     SDL_Color color = {255, 255, 255};

//     auto p_result_surface = _CreateSurface(800, 800);

//     int x = 0;
//     auto const &glyph_infos = pFontMapInfo->GetGlyphInfos();
//     for(size_t i = 0, len = glyph_infos.size(); i < len; ++i) {
//         auto p_glyph_info = glyph_infos[i];
//         auto ch = mcow::StringUtils::ToString(p_glyph_info->c).c_str();

//         auto p_glyph_surface = TTF_RenderText_Blended(
//             p_ttf_font,
//             ch,
//             color
//         );

//         SDL_Rect dst_rect;
//         dst_rect.x = x   + p_glyph_info->min_x;
//         dst_rect.y = 100; //- p_glyph_info->max_y + p_glyph_info->font_accent;
//         x += p_glyph_info->advance;

//         SDL_FillRect(
//             p_result_surface,
//             &dst_rect,
//             SDL_MapRGB(p_result_surface->format, 255, 0, 255)
//         );

//         SDL_BlitSurface(
//             p_glyph_surface,
//             nullptr,
//             p_result_surface,
//             &dst_rect
//         );
//         _SafeDestroySurface(p_glyph_surface);

// //        if(i > 2) {
// //        break;
// //        }
//     }

//     auto p_sdl_texture = SDL_CreateTextureFromSurface(g_pRenderer, p_result_surface);
//     _SafeDestroySurface(p_result_surface);

//     return CastSDL(p_sdl_texture);
// }

//------------------------------------------------------------------------------
Graphics::GlyphInfo_t*
Graphics::CreateGlyphInfo(char c, Font_t *pFont)
{
    auto p_ttf_font = CastSDL(pFont);
    auto p_info = mcow::Memory::AllocClear<GlyphInfo_t>(1);

    p_info->c = c;
    p_info->p_font      = pFont;
    p_info->font_accent = TTF_FontAscent(p_ttf_font);

    TTF_GlyphMetrics(
        p_ttf_font,
        c,
        &p_info->min_x,
        &p_info->max_x,
        &p_info->min_y,
        &p_info->max_y,
        &p_info->advance
    );

    return p_info;
}

//------------------------------------------------------------------------------
void
Graphics::DestroyGlyphInfo(GlyphInfo_t *pGlyphInfo)
{
    mcow::Memory::Free(pGlyphInfo);
}

//------------------------------------------------------------------------------
Graphics::Texture_t*
Graphics::CreateGlyphTexture(GlyphInfo_t *pGlyphInfo)
{
    auto p_ttf_font = CastSDL(pGlyphInfo->p_font);
    SDL_Color color = {255, 255, 255};

    auto ch = mcow::StringUtils::ToString(pGlyphInfo->c).c_str();

    auto p_glyph_surface = TTF_RenderText_Blended(p_ttf_font, ch, color);
    auto p_sdl_texture   = SDL_CreateTextureFromSurface(g_pRenderer, p_glyph_surface);

    _SafeDestroySurface(p_glyph_surface);

    return CastSDL(p_sdl_texture);
}
