
// Header
#include "../include/Packer.h"
// mcow
#include "mcow/Algo/Algo.hpp"
#include "mcow/Memory/Memory.hpp"
#include "mcow/CodeUtils/FakeKeywords.hpp"
// glypher
#include "../include/Utils.h"

//----------------------------------------------------------------------------//
// Private Types                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
struct Node_t {
    bool used = false;
    Rect_t rect;

    Node_t *p_right = nullptr;
    Node_t *p_down  = nullptr;
}; // struct Node_t

//----------------------------------------------------------------------------//
// Private Vars                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
Node_t *g_pRootNode = nullptr;


//----------------------------------------------------------------------------//
// Private Functions                                                          //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
mcow_internal_function Node_t*
AllocNode(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
    auto p_node = mcow::Memory::AllocClear<Node_t>(1);
    p_node->rect.x = x;
    p_node->rect.y = y;
    p_node->rect.w = w;
    p_node->rect.h = h;

    return p_node;
}

//------------------------------------------------------------------------------
mcow_internal_function void
FreeNode(Node_t *pNode)
{
    if(pNode) {
        FreeNode(pNode->p_right);
        FreeNode(pNode->p_down);
        mcow::Memory::Free(pNode);
    }
}

//------------------------------------------------------------------------------
mcow_internal_function Node_t*
FindNode(Node_t* pNode, uint32_t w, uint32_t h)
{
    MCOW_ASSERT_NOT_NULL(pNode);

    if(pNode->used) {
        auto p_found_node = FindNode(pNode->p_right, w, h);

        if(p_found_node) {
            return p_found_node;
        }

        return FindNode(pNode->p_down, w, h);
    } else if(w <= pNode->rect.w && h <= pNode->rect.h) {
        return pNode;
    }

    return nullptr;
}

//------------------------------------------------------------------------------
mcow_internal_function Node_t*
SplitNode(Node_t* pNode, uint32_t w, uint32_t h)
{
    MCOW_ASSERT_NOT_NULL(pNode);
    pNode->used = true;

    const auto &r = pNode->rect;
    pNode->p_down  = AllocNode(r.x    , r.y + h, r.w    , r.h - h);
    pNode->p_right = AllocNode(r.x + w, r.y    , r.w - w, h      );

    return pNode;
}

//------------------------------------------------------------------------------
mcow_internal_function Node_t*
GrowNode_Right(uint32_t w, uint32_t h)
{
    auto const &r = g_pRootNode->rect;

    auto p_new_root = AllocNode(0, 0, r.w + w, r.h);

    p_new_root->used    = true;
    p_new_root->p_down  = g_pRootNode;
    p_new_root->p_right = AllocNode(r.w, 0, w, r.h);

    g_pRootNode = p_new_root;

    auto p_node = FindNode(g_pRootNode, w, h);
    if(p_node) {
        return SplitNode(p_node, w, h);
    }

    return nullptr;
}

//------------------------------------------------------------------------------
mcow_internal_function Node_t*
GrowNode_Down(uint32_t w, uint32_t h)
{
    auto const &r = g_pRootNode->rect;

    auto p_new_root = AllocNode(0, 0, r.w, r.h + h);

    p_new_root->used    = true;
    p_new_root->p_down  = AllocNode(0, r.h, r.w, h);
    p_new_root->p_right = g_pRootNode;

    g_pRootNode = p_new_root;

    auto p_node = FindNode(g_pRootNode, w, h);
    if(p_node) {
        return SplitNode(p_node, w, h);
    }

    return nullptr;
}

//------------------------------------------------------------------------------
mcow_internal_function Node_t*
GrowNode(uint32_t w, uint32_t h)
{
    // Just to reduce verbosity.
    auto const root_w = g_pRootNode->rect.w;
    auto const root_h = g_pRootNode->rect.h;

    auto can_grow_down  = (w <= root_w);
    auto can_grow_right = (h <= root_h);

    // Attempt to keep square-ish by growing right
    // when height is much greater than width
    auto should_grow_right = can_grow_right && (root_h >= (root_w + w));

    // Attempt to keep square-ish by growing down
    // when width is much greater than height
    auto should_grow_down = can_grow_down && (root_w >= (root_h + h));

    if     (should_grow_right) return GrowNode_Right(w, h);
    else if(should_grow_down ) return GrowNode_Down (w, h);
    else if(can_grow_right   ) return GrowNode_Right(w, h);
    else if(can_grow_down    ) return GrowNode_Down (w, h);
    else                       return nullptr;
}

//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Packer::Pack(
    std::vector<Rect_t*> rects,
    uint32_t             padding,
    uint32_t            *pResultWidthOut,
    uint32_t            *pResultHeightOut)
{
    mcow::Algo::Sort(rects, [](Rect_t *pR1, Rect_t *pR2){
        return pR1->w > pR2->w;
    });

    auto const *p_bigger_rect = rects.front();

    g_pRootNode = AllocNode(
        0, 0,
        p_bigger_rect->w + padding,
        p_bigger_rect->h + padding
    );

    for(auto p_rect : rects) {
        auto rect_w = p_rect->w + padding;
        auto rect_h = p_rect->h + padding;

        auto p_node = FindNode(g_pRootNode, rect_w, rect_h);
        if(!p_node) {
            p_node = GrowNode(rect_w, rect_h);
        } else {
            SplitNode(p_node, rect_w, rect_h);
        }

        p_rect->x = (p_node->rect.x + padding);
        p_rect->y = (p_node->rect.y + padding);

        *pResultWidthOut  = std::max(*pResultWidthOut,  p_rect->x + p_rect->w);
        *pResultHeightOut = std::max(*pResultHeightOut, p_rect->y + p_rect->h);
    }

    FreeNode(g_pRootNode);
}
