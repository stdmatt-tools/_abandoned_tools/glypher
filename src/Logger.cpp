// Header
#include "../include/Logger.h"
// std
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// Usings
using namespace mcow;

//----------------------------------------------------------------------------//
// MACROS                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
#define _LOG(_lvl_)                      \
    do {                                 \
        printf("%s ", _lvl_);            \
        va_list _list;                   \
        va_start(_list, pFormat);        \
                vprintf(pFormat, _list); \
        va_end(_list);                   \
        printf("\n");                    \
    } while(0);


//----------------------------------------------------------------------------//
// Variables                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
static uint32_t g_LogLevel = Logger::LogLevel::LOG_LEVEL_NONE;

static inline bool
CanLog(Logger::LogLevel level)
{
    if(g_LogLevel == Logger::LogLevel::LOG_LEVEL_NONE) {
        return false;
    }

    return (g_LogLevel & Logger::LogLevel::LOG_LEVEL_VERBOSE)
        || (g_LogLevel & level);
}

//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Logger::SetLogLevel(Logger::LogLevel level)
{
    g_LogLevel = level;
}


//------------------------------------------------------------------------------
void
Logger::AddLogLevel(Logger::LogLevel level)
{
    if(level == LogLevel::LOG_LEVEL_NONE ||
       level == LogLevel::LOG_LEVEL_VERBOSE)
    {
        g_LogLevel = level;
        return;
    }

    g_LogLevel |= level;
}

//------------------------------------------------------------------------------
void
Logger::RemoveLogLevel(Logger::LogLevel level)
{
    if(level == LogLevel::LOG_LEVEL_NONE) {
        g_LogLevel = LOG_LEVEL_VERBOSE;
        return;
    }

    g_LogLevel &= (~level);
}

// //------------------------------------------------------------------------------
// void
// Logger::D(char const * const pFormat, ...)
// {
//     if(!CanLog(LogLevel::LOG_LEVEL_DEBUG)) {
//         return;
//     }

//     _LOG("[DEBUG]");
// }

// //------------------------------------------------------------------------------
// void
// Logger::I(char const * const pFormat, ...)
// {
//     if(!CanLog(LogLevel::LOG_LEVEL_INFO)) {
//         return;
//     }

//     _LOG("[INFO]");
// }

// //------------------------------------------------------------------------------
// void
// Logger::W(char const * const pFormat, ...)
// {
//     if(!CanLog(LogLevel::LOG_LEVEL_WARNING)) {
//         return;
//     }

//     _LOG("[WARNING]");
// }

// //------------------------------------------------------------------------------
// void
// Logger::E(char const * const pFormat, ...)
// {
//     if(!CanLog(LogLevel::LOG_LEVEL_ERROR)) {
//         return;
//     }

//     _LOG("[ERROR]");
// }

// //------------------------------------------------------------------------------
// void
// Logger::F(char const * const pFormat, ...)
// {
//     _LOG("[FATAL]");
//     abort();
// }
